#+TITLE: Error
#+DATE: <2018-02-24 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:t
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t toc:t todo:t |:t
#+CREATOR: Emacs 25.2.2 (Org mode 8.2.10)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: id
#+SELECT_TAGS: export

* Error

module perl, yang dibutuhkan oleh package /[[file:git.org][git]]/.

** source

http://www.linuxfromscratch.org/blfs/view/systemd/general/perl-modules.html#perl-error

** konfigurasi

#+BEGIN_SRC sh
perl Makefile.PL &&
make &&
make test
make install

#+END_SRC
