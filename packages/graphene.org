#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: graphene
#+DATE: <2018-04-02 Mon>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.9)

* graphene

** source
#+begin_src sh :dir /usr/src/graphene :results output
git remote -v
#+end_src

#+RESULTS:
: origin	git@github.com:ebassi/graphene.git (fetch)
: origin	git@github.com:ebassi/graphene.git (push)

** konfigurasi
#+begin_src sh :dir /usr/src/graphene :results output
meson builddir --prefix /usr  -Dgtk_doc=true &&
ninja -C builddir &&
cd builddir &&
meson test &&
cd .. &&
sudo ninja -C builddir install
#+end_src

** dependencies
glib2, git, gobject-introspection, gtk-doc, meson

* NOTES

** installed <2018-04-02 Mon>
#+begin_src sh :dir /usr/src/graphene :results output
git log -1
#+end_src

#+RESULTS:
: commit 058544289496f98c19cb6bd5e7eaddad76c55a5a
: Author: Rico Tzschichholz <ricotz@ubuntu.com>
: Date:   Tue Mar 6 10:34:40 2018 +0100
: 
:     Fix header reference in GIR
