#+TITLE: libpng
#+DATE: <2018-02-25 Sun>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:t
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t toc:t todo:t |:t
#+CREATOR: Emacs 25.2.2 (Org mode 8.2.10)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: id
#+SELECT_TAGS: export

* libpng

** source

http://www.linuxfromscratch.org/blfs/view/systemd/general/libpng.html

** konfigurasi

#+BEGIN_SRC sh
gzip -cd ../libpng-1.6.34-apng.patch.gz | patch -p1
#+END_SRC

#+BEGIN_SRC sh
LIBS=-lpthread ./configure --prefix=/usr --disable-static &&
make

make install &&
mkdir -v /usr/share/doc/libpng-1.6.34 &&
cp -v README libpng-manual.txt /usr/share/doc/libpng-1.6.34

#+END_SRC
