#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: enchant
#+DATE: <2018-03-31 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.9)

* enchant

** source
- https://abiword.github.io/enchant/

#+begin_src sh :dir /usr/src/enchant :results output
git remote -v
#+end_src

#+RESULTS:
: origin	git@github.com:AbiWord/enchant.git (fetch)
: origin	git@github.com:AbiWord/enchant.git (push)

** konfigurasi

#+begin_src sh :dir /usr/src/enchant :results output
./bootstrap &&
./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
--disable-static --enable-relocatable &&
make &&
sudo make install
#+end_src

** dependencies
unittest-cpp, aspell, hunspell, applespell, voikko

* NOTES

