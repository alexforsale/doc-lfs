#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: python-yaml
#+DATE: <2018-03-25 Sun>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* pyyaml

** source
#+BEGIN_SRC sh :dir /usr/src/pyyaml :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git@github.com:yaml/pyyaml.git (fetch)
: origin	git@github.com:yaml/pyyaml.git (push)

** konfigurasi
#+BEGIN_SRC sh
python setup.py build &&
sudo python setup.py install --optimize=1 &&
python3 setup.py build &&
sudo python3 setup.py install --optimize=1
#+END_SRC

* NOTES

** installed <2018-03-25 Sun>
#+BEGIN_SRC sh :dir /usr/src/pyyaml :results output
git log -1
#+END_SRC

#+RESULTS:
#+begin_example
commit 298e07907ae526594069f6fdf31f2f1278cc1ae3
Author: Donald Stufft <donald@stufft.io>
Date:   Fri Sep 8 11:05:24 2017 -0400

    Fallback to Pure Python if Compilation fails
    
    Originally this code attempted to determine if compiling the C ext
    would succeed, and if it thought it should, it would then require that
    the C extension succeed in order to install. This fails in cases where
    the detection code passes, but compiling ultimately fails (one instance
    this might happen is if the Python headers are not installed).
    
    Instead of "asking permission", this code will now just attempt to
    compile the module, and will fall back to pure Python if that fails,
    unless the person has explicitly asked for the C module, in which case
    it will still just fail.
#+end_example
