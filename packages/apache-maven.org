#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: apache-maven
#+DATE: <2018-03-23 Fri>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* apache maven

** source
#+BEGIN_SRC sh :dir /usr/src/maven :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	https://github.com/apache/maven.git (fetch)
: origin	https://github.com/apache/maven.git (push)

*** binary (untuk bootstraping)
Proses ini hanya perlu dilakukan pertama kali
https://archive.apache.org/dist/maven/maven-3/3.5.3/binaries/apache-maven-3.5.3-bin.tar.gz

** konfigurasi

- Unpack binary
#+BEGIN_SRC sh
wget https://archive.apache.org/dist/maven/maven-3/3.5.3/binaries/apache-maven-3.5.3-bin.tar.gz
install -vdm 755 apache-maven-bin     &&
tar -xf apache-maven-3.5.3-bin.tar.gz \
    --strip-components=1              \
    --directory apache-maven-bin      &&
SAVEPATH=$PATH   &&
PATH=apache-maven-bin/bin:$PATH
#+END_SRC

- compile
#+BEGIN_SRC sh
mvn -DdistributionTargetDir=build \
    package
#+END_SRC
