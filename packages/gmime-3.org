#+TITLE: gmime-3
#+DATE: <2018-02-25 Sun>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:t
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t toc:t todo:t |:t
#+CREATOR: Emacs 25.2.2 (Org mode 8.2.10)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: id
#+SELECT_TAGS: export

* gmime-3

** source

http://www.linuxfromscratch.org/blfs/view/systemd/general/gmime3.html

#+BEGIN_SRC sh :dir /usr/src/gmime :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git@github.com:GNOME/gmime.git (fetch)
: origin	git@github.com:GNOME/gmime.git (push)

** konfigurasi

#+BEGIN_SRC sh
./autogen.sh --prefix=/usr --disable-static --enable-gtk-doc &&
make &&
sudo make install
#+END_SRC

** dependencies

*** Required

[[file:glib.org][GLib-2.54.3]] and [[file:libgpg-error.org][libgpg-error-1.27]]

*** Recommended

[[file:gobject-intropection.org][gobject-introspection-1.54.1]], [[file:libidn.org][libidn-1.33]], and [[file:vala.org][Vala-0.38.8]]

*** Optional

[[file:docbook-util.org][DocBook-utils-0.6.14]], [[file:gpgme.org][GPGME-1.10.0]], [[file:gtk-doc.org][GTK-Doc-1.27]] and Gtk# (requires
Mono)
 
