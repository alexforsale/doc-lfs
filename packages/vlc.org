#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: vlc
#+DATE: <2018-03-08 Thu>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* TODO vlc

** source

#+BEGIN_SRC sh :dir /usr/src/vlc :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git://git.videolan.org/vlc.git (fetch)
: origin	git://git.videolan.org/vlc.git (push)

** konfigurasi

#+BEGIN_SRC sh
./bootstrap &&
sed -i '/vlc_demux.h/a #define LUA_COMPAT_APIINTCASTS' modules/lua/vlc.h   &&
sed -i '/DEPRECATED/s:^://:'  modules/text_renderer/freetype/text_layout.c &&
BUILDCC=gcc ./configure --prefix=/usr --disable-static \
--disable-opencv --disable-live555 --enable-update-check --disable-qt &&
./compile && make check &&
sudo make install &&
sudo gtk-update-icon-cache &&
sudo update-desktop-database
#+END_SRC
** dependencies

*** Recommended
[[file:alsa-lib.org][alsa-lib-1.1.5]], [[file:FFmpeg.org][FFmpeg-3.4.2]], [[file:liba52.org][liba52-0.7.4]], [[file:libgcrypt.org][libgcrypt-1.8.2]]
[[file:libmad.org][libmad-0.15.1b]], [[file:lua.org][Lua-5.3.4]], and [[file:~/Documents/org/lfs/jalan-panjang-menuju-X.org][X Window System]] (wayland-egl),

*** Optional features and packages
[[file:dbus.org][dbus-1.12.6]]

*** Optional input plugins
[[file:libcddb.org][libcddb-1.3.2]], [[file:libdv.org][libdv-1.0.0]], [[file:libdvdcss.org][libdvdcss-1.4.1]], [[file:libdvdread.org][libdvdread-6.0.0]],
[[file:libdvdnav.org][libdvdnav-6.0.0]], [[file:opencv.org][opencv-3.4.1]] (currently broken), [[file:samba.org][Samba-4.7.5]],
[[file:v4l-utils.org][v4l-utils-1.14.2]], [[file:libbluray.org][libbluray]], [[file:libdc1394.org][libdc1394]], [[file:libproxy.org][libproxy]], Live555, and
[[file:VCDImager.org][VCDImager]] (requires [[file:libcdio.org][libcdio-2.0.0]])

*** Optional mux/demux plugins
[[file:libogg.org][libogg-1.3.3]], Game Music Emu, [[file:libdvbpsi.org][libdvbpsi]], [[file:icecast-server.org][libshout]], [[file:matroska.org::*libmatroska][libmatroska]]
(requires [[file:matroska.org::*libebml][libebml]]), [[file:libmodplug.org][libmodplug]], [[file:musepack.org][Musepack]], and sidplay-libs

*** Optional codec plugins
[[file:FAAD2.org][FAAD2-2.8.8]], [[file:FLAC.org][FLAC-1.3.2]], [[file:libass.org][libass-0.14.0]], libmpeg2-0.5.1, [[file:libpng.org][libpng-1.6.34]],
[[file:libtheora.org][libtheora-1.1.1]], [[file:libva.org][libva-2.1.0]], [[file:libvorbis.org][libvorbis-1.3.5]], [[file:opus.org][Opus-1.2.1]],
[[file:speex.org][Speex-1.2.0]], [[file:x264.org][x264-20180212-2245]], Dirac, FluidSynth, [[file:libdca.org][libdca]], libkate,
OpenMAX, [[file:schroedinger.org][Schroedinger]], [[file:usbutils.org][Tremor]], [[file:twolame.org][Twolame]], and [[file:ZVBI.org][Zapping VBI]]

*** Optional video plugins
[[file:AAlib.org][AAlib-1.4rc5]], [[file:fontconfig.org][Fontconfig-2.13.0]], [[file:freetype.org][FreeType-2.9]], [[file:fribidi.org][FriBidi-1.0.1]],
[[file:librsvg.org][librsvg-2.42.2]], [[file:libvdpau.org][libvdpau-1.1.1]], [[file:SDL.org][SDL-1.2.15]] (with [[file:SDL_image.org][SDL_image]]), and
[[file:libcaca.org][libcaca]]

*** Optional audio plugins
[[file:pulseaudio.org][PulseAudio-11.1]], [[file:libsamplerate.org][libsamplerate-0.1.9]], and [[file:jack2.org][JACK]]

*** Optional interface plugins
[[file:QT5.org][Qt-5.10.1]] (required for the graphical user interface), libtar, and
[[file:lirc.org][LIRC]]

*** Optional visualisations and video filter plugins
Goom and projectM

*** Optional service discovery plugins
[[file:avahi.org][Avahi-0.7]], [[file:libmtp.org][libmtp]] and [[file:libupnp.org][libupnp]]

*** Miscellaneous options
[[file:gnutls.org][GnuTLS-3.6.2]], [[file:libnotify.org][libnotify-0.7.7]], [[file:libxml2.org][libxml2-2.9.7]], [[file:taglib.org][taglib-1.11.1]],
[[file:xdg-utils.org][xdg-utils-1.1.2]] (runtime), and AtmoLight, [[file:libplacebo.org][libplacebo]], [[file:srt.org][srt]]
[[file:microdns.org][microdns]]

*** dependencies versi archlinux
https://www.archlinux.org/packages/extra/x86_64/vlc/
