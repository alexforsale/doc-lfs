#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: io
#+DATE: <2018-03-04 Sun>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* io

** source

#+BEGIN_SRC sh :dir /usr/src/io :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	https://github.com/stevedekorte/io.git (fetch)
: origin	https://github.com/stevedekorte/io.git (push)

** konfigurasi

#+BEGIN_SRC sh
mkdir -pv build && cd $_ &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
-DCMAKE_BUILD_TYPE=release .. &&
make &&
sudo make install
#+END_SRC

** dependencies

[[file:ode.org][ode]], [[file:memcached.org][memcached]]

* DONE fix build
  CLOSED: [2018-03-12 Mon 17:24]
  - State "DONE"       from "TODO"       [2018-03-12 Mon 17:24] \\
    fix compiling, kompilasi ode perlu instalasi library manual(cp -rv dll)
