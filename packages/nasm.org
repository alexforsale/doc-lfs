#+TITLE: nasm
#+DATE: <2018-02-25 Sun>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:t
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t toc:t todo:t |:t
#+CREATOR: Emacs 25.2.2 (Org mode 8.2.10)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: id
#+SELECT_TAGS: export

* nasm

** source

http://www.linuxfromscratch.org/blfs/view/systemd/general/nasm.html

#+BEGIN_SRC sh :dir /usr/src/nasm :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git://repo.or.cz/nasm.git (fetch)
: origin	git://repo.or.cz/nasm.git (push)

** konfigurasi

#+BEGIN_SRC sh
./autogen.sh
./configure --prefix=/usr --enable-ccache &&
make everything && sudo make install
#+END_SRC

** dependencies

perl module Font::TTF::Font (cpan Font::TTF::Font), Sort::Versions
(cpan Sort::Versions)

* NOTES

** installed on <2018-03-22 Thu>
#+BEGIN_SRC sh :dir /usr/src/nasm :results output
git log -1
#+END_SRC

#+RESULTS:
: commit dd6a2cdcf4a4bbd2c64fe0dbdcd3e0a2c8d63032
: Author: Cyrill Gorcunov <gorcunov@gmail.com>
: Date:   Sun Feb 25 22:25:57 2018 +0300
: 
:     Revert "compiler: Add fallthrough() helper"
:     
:     This reverts commit 8ba28e13ea4453a587d08e5533e60f4ff2b4781a.
