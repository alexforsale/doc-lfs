#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: libconfig
#+DATE: <2018-03-01 Thu>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* libconfig

** source

#+BEGIN_SRC sh :dir /usr/src/libconfig :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	https://github.com/hyperrealm/libconfig.git (fetch)
: origin	https://github.com/hyperrealm/libconfig.git (push)

** konfigurasi

#+BEGIN_SRC sh
mkdir -v build &&
cd       build &&
cmake -DCMAKE_INSTALL_PREFIX=/usr .. &&
make &&
sudo make install
#+END_SRC
