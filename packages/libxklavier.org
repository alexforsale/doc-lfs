#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: libxklavier
#+DATE: <2018-03-31 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.9)

* libxklavier

** source
#+begin_src sh :dir /usr/src/libxklavier :results output
git remote -v
#+end_src

#+RESULTS:
: origin	git://anongit.freedesktop.org/libxklavier (fetch)
: origin	git://anongit.freedesktop.org/libxklavier (push)

** konfigurasi
#+begin_src sh :dir /usr/src/libxklavier :results output
./autogen.sh --prefix=/usr --disable-static --enable-vala=yes \
--enable-gtk-doc --enable-xkb-support --enable-xmodmap-support &&
make && make check &&
sudo make install
#+end_src

** dependencies

***  Required

GLib-2.56.0, ISO Codes-3.77, libxml2-2.9.8 and Xorg Libraries

*** Recommended

gobject-introspection-1.56.0

*** Optional

GTK-Doc-1.28 and Vala-0.40.0

* NOTES

** installed <2018-03-31 Sat>
#+begin_src sh :dir /usr/src/libxklavier :results output
git log -1
#+end_src

#+RESULTS:
: commit 65c132a65e90a42e898f07243ef544109ada53c9
: Author: Sergey V. Udaltsov <svu@gnome.org>
: Date:   Tue Jun 25 01:00:12 2013 +0100
: 
:     pushed soname
:     
:     -xkl_engine_VOID__FLAGS_INT_BOOLEAN
:     +xkl_engine_VOID__ENUM_INT_BOOLEAN
:     so public API changed
