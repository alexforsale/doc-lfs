#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: transmission
#+DATE: <2018-03-09 Fri>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* transmission

** source

#+BEGIN_SRC sh :dir /usr/src/transmission :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git@github.com:transmission/transmission.git (fetch)
: origin	git@github.com:transmission/transmission.git (push)

** konfigurasi

#+BEGIN_SRC sh
git submodule init &&
git submodule update &&
./autogen.sh
./configure --prefix=/usr --localstatedir=/var --sysconfdir=/etc \
--disable-static --enable-cli --enable-daemon --with-systemd --with-gtk &&
make &&
sudo make install
#+END_SRC
