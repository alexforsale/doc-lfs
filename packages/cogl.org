#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: cogl
#+DATE: <2018-04-02 Mon>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.9)

* cogl

** source
#+begin_src sh :dir /mnt/sources/cogl :results output
git remote -v
#+end_src

#+RESULTS:
: origin	git@github.com:GNOME/cogl.git (fetch)
: origin	git@github.com:GNOME/cogl.git (push)

** konfigurasi
#+begin_src sh :dir /mnt/sources/cogl :results output
NOCONFIGURE=1 ./autogen.sh &&
./configure --prefix=/usr --enable-gles{1,2} \
--enable-{kms,wayland}-egl-platform \
--enable-wayland-egl-server &&
sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool &&
make &&
sudo make install
#+end_src

** dependencies
mesa libdrm libxext libxdamage libxcomposite gdk-pixbuf2 pango
libxrandr

* NOTES

** installed <2018-04-02 Mon>
#+begin_src sh :dir /mnt/sources/cogl :results output
git log -1
#+end_src

#+RESULTS:
: commit 74af537a7fc33ae052e444ad5cad3b1d6775a18d
: Author: gogo <trebelnik2@gmail.com>
: Date:   Sat Mar 17 14:51:16 2018 +0000
: 
:     Add Croatian translation
:     
:     (cherry picked from commit c35952a699a55cc3af23718c875ac251a01fbd38)
