#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: double-conversion
#+DATE: <2018-03-09 Fri>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* double-conversion

** source

#+BEGIN_SRC sh :dir /usr/src/double-conversion :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git@github.com:google/double-conversion.git (fetch)
: origin	git@github.com:google/double-conversion.git (push)

** konfigurasi

#+BEGIN_SRC sh
scons &&
sudo scons prefix=/usr install
#+END_SRC
