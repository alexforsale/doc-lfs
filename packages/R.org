#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: R
#+DATE: <2018-03-03 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* R

** source

http://www.r-project.org/

** konfigurasi

#+BEGIN_SRC sh
./tools/rsync-recommended
#+END_SRC

#+BEGIN_SRC sh
./configure --prefix=/usr --with-x &&
make &&
make check &&
make info && make pdf &&
sudo make install &&
sudo make install-info &&
sudo make install-pdf
#+END_SRC

** dependencies

[[file:gcc.org][GCC]] dengan fortran compiler

*** DONE recompile GCC
    CLOSED: [2018-03-12 Mon 20:01]
    - State "DONE"       from "TODO"       [2018-03-12 Mon 20:01]

* Version

#+BEGIN_SRC sh :results output
R --version
#+END_SRC

#+RESULTS:
#+begin_example
R Under development (unstable) (2018-03-12 r74387) -- "Unsuffered Consequences"
Copyright (C) 2018 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under the terms of the
GNU General Public License versions 2 or 3.
For more information about these matters see
http://www.gnu.org/licenses/.

#+end_example
