#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: mujs
#+DATE: <2018-03-15 Thu>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* mujs

** source

#+BEGIN_SRC sh :dir /usr/src/mujs :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git://git.ghostscript.com/mujs.git (fetch)
: origin	git://git.ghostscript.com/mujs.git (push)

** konfigurasi

#+BEGIN_SRC sh
make release &&
sudo make prefix=/usr install
#+END_SRC
