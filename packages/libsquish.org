#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: libsquish
#+DATE: <2018-03-11 Sun>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* libSquish

** source

https://sourceforge.net/projects/libsquish/

** konfigurasi

#+BEGIN_SRC sh
mkdir -pv build && cd $_ &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
-DBUILD_SHARED_LIBS=ON .. &&
make &&
sudo make install
#+END_SRC
