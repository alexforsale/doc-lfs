#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: judy
#+DATE: <2018-03-23 Fri>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* judy

** source
https://sourceforge.net/p/judy

** konfigurasi

#+BEGIN_SRC sh
autoreconf -fvi &&
./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
--disable-static &&
make && make check &&
sudo make install
#+END_SRC

* NOTES

** installed <2018-03-23 Fri>
#+BEGIN_SRC sh :dir /usr/src/judy-code :results output
svn log -l 1
#+END_SRC

#+RESULTS:
: ------------------------------------------------------------------------
: r57 | troyhebe | 2007-05-23 02:57:05 +0700 (Wed, 23 May 2007) | 1 line
: 
: forgot to checkin the final X-bit changes
: ------------------------------------------------------------------------
