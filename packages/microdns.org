#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: microdns
#+DATE: <2018-03-28 Wed>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* libmicrodns

** source
#+BEGIN_SRC sh :dir /usr/src/libmicrodns :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git@github.com:videolabs/libmicrodns.git (fetch)
: origin	git@github.com:videolabs/libmicrodns.git (push)

** konfigurasi
#+BEGIN_SRC sh
./bootstrap &&
./configure --prefix=/usr --localstatedir=/var --sysconfdir=/etc \
--disable-static &&
make &&
sudo make install
#+END_SRC

* NOTES

** installed <2018-03-28 Wed>
#+BEGIN_SRC sh :dir /usr/src/libmicrodns :results output
git log -1
#+END_SRC

#+RESULTS:
: commit b96302aed59d82004a2d7ddb59dc69439c9fffdb
: Author: Hugo Beauzée-Luyssen <hugo@beauzee.fr>
: Date:   Mon Mar 19 15:17:24 2018 +0100
: 
:     Bump version to 0.0.10

