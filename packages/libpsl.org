#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: libpsl
#+DATE: <2018-02-28 Wed>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* libpsl

** source

#+BEGIN_SRC sh :dir /usr/src/libpsl :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git@github.com:rockdaboot/libpsl.git (fetch)
: origin	git@github.com:rockdaboot/libpsl.git (push)

** konfigurasi

#+BEGIN_SRC sh
./autogen.sh &&
./configure --prefix=/usr --disable-static --enable-gtk-doc &&
make && make check &&
sudo make install
#+END_SRC

* NOTES

** updated <2018-03-23 Fri>
#+BEGIN_SRC sh :dir /usr/src/libpsl :results output
git log -1
#+END_SRC

#+RESULTS:
: commit fe2042fea8652b5d14824dfd9e0580ed18cbf133
: Author: Tim Rühsen <tim.ruehsen@gmx.de>
: Date:   Tue Mar 6 10:21:45 2018 +0100
: 
:     Fix make distcheck without enabled docs
