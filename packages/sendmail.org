#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: sendmail
#+DATE: <2018-03-24 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* sendmail

** source
- http://www.linuxfromscratch.org/blfs/view/systemd/server/sendmail.html
- https://www.proofpoint.com/us/open-source-email-solution

** konfigurasi

** dependencies

***  Required

[[file:openldap.org][OpenLDAP-2.4.45 (client)]]

*** Recommended

[[file:openssl.org][OpenSSL-1.0.2n]] Libraries and [[file:cyrus-sasl.org][Cyrus SASL-2.1.26]]

*** Optional

[[file:ghostscript.org][ghostscript-9.22]] (for creating PDF documentation), [[file:procmail.org][Procmail-3.22]] (the
configuration proposed below requires that procmail be present at
run-time), and [[file:nph.org][nph]]
