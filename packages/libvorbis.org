#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: libvorbis
#+DATE: <2018-02-28 Wed>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* libvorbis

** source

http://www.linuxfromscratch.org/blfs/view/systemd/multimedia/libvorbis.html

** konfigurasi

#+BEGIN_SRC sh
sed -i '/components.png \\/{n;d}' doc/Makefile.in &&

./configure --prefix=/usr --disable-static &&
make &&

sudo make install &&
sudo install -v -m644 doc/Vorbis* /usr/share/doc/libvorbis-1.3.5

#+END_SRC

** dependencies

*** Required 

[[file:libogg.org][libogg-1.3.3]]

*** Optional 

[[file:doxygen.org][Doxygen-1.8.14]] and texlive-20170524 (or install-tl-unx) (specifically,
pdflatex and htlatex) to build the PDF documentation
