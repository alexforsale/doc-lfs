#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: daq
#+DATE: <2018-03-27 Tue>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* daq

** source
https://www.snort.org/downloads

** konfigurasi
#+BEGIN_SRC sh
./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
--disable-static --with-dnet-includes=/usr/include/dnet \
--with-dnet-libraries=/usr/lib/ &&
make &&
sudo make install
#+END_SRC

* NOTES

** installed <2018-03-27 Tue>

- tarball version 2.0.6
