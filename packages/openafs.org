#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: openafs
#+DATE: <2018-03-08 Thu>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* openafs

** source

#+BEGIN_SRC sh :dir /usr/src/openafs :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git://git.openafs.org/openafs.git (fetch)
: origin	git://git.openafs.org/openafs.git (push)

** konfigurasi

#+BEGIN_SRC sh
./regen.sh
./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
--with-afs-sysname=amd64_linux26 --disable-kernel-module \
--with-libintl=/usr --with-roken=/usr --with-krb5=/usr --with-gssapi=/usr &&
make &&
sudo make install
#+END_SRC

** TODO undefined reference to `base64_encode'
