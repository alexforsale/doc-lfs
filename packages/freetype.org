#+TITLE: freetype
#+DATE: <2018-02-25 Sun>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:t
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t toc:t todo:t |:t
#+CREATOR: Emacs 25.2.2 (Org mode 8.2.10)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: id
#+SELECT_TAGS: export

* freetype

** source

http://www.linuxfromscratch.org/blfs/view/systemd/general/freetype2.html

** konfigurasi

#+BEGIN_SRC 
tar -xf ../freetype-doc-2.9.tar.bz2 --strip-components=2 -C docs
#+END_SRC

*** sebelum instalasi harfbuzz

#+BEGIN_SRC sh
sed -ri "s:.*(AUX_MODULES.*valid):\1:" modules.cfg &&

sed -r "s:.*(#.*SUBPIXEL_RENDERING) .*:\1:" \
    -i include/freetype/config/ftoption.h  &&

./configure --prefix=/usr --disable-static --without-harfbuzz &&
make
#+END_SRC

*** setelah instalasi harfbuzz

#+BEGIN_SRC sh
sed -ri "s:.*(AUX_MODULES.*valid):\1:" modules.cfg &&

sed -r "s:.*(#.*SUBPIXEL_RENDERING) .*:\1:" \
    -i include/freetype/config/ftoption.h  &&

./configure --prefix=/usr --disable-static &&
make
#+END_SRC

#+BEGIN_SRC sh
make install &&
install -v -m755 -d /usr/share/doc/freetype-2.9 &&
cp -v -R docs/*     /usr/share/doc/freetype-2.9

#+END_SRC

** dependencies

*** Recommended

[[file:harfbuzz.org][HarfBuzz-1.7.5]] (first, install without it, after it is installed,
reinstall FreeType-2.9), [[file:libpng.org][libpng-1.6.34]], and [[file:which.org][Which-2.21]]
