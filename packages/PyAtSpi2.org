#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: PyAtSpi2
#+DATE: <2018-02-27 Tue>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* PyAtSpi2

** source

http://www.linuxfromscratch.org/blfs/view/systemd/general/python-modules.html#pyatspi2

** konfigurasi

#+BEGIN_SRC sh
mkdir python2 &&
pushd python2 &&
../configure --prefix=/usr --with-python=/usr/bin/python &&
make &&
popd

mkdir python3 &&
pushd python3 &&
../configure --prefix=/usr --with-python=/usr/bin/python3 &&
make &&
popd

sudo make -C python2 install
sudo make -C python3 install

#+END_SRC

** dependencies

*** Required 

[[file:pygobject.org][PyGObject-3.26.1]]

*** Recommended 

[[file:at-spi2-core.org][at-spi2-core-2.26.2]]
