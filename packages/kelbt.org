#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: kelbt
#+DATE: <2018-03-31 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.9)

* kelbt

** source
#+begin_src sh :dir /usr/src/kelbt :results output
git remote -v
#+end_src

#+RESULTS:
: origin	git@github.com:Distrotech/kelbt.git (fetch)
: origin	git@github.com:Distrotech/kelbt.git (push)

** konfigurasi
#+begin_src sh :dir /usr/src/kelbt :results output
./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var &&
make &&
sudo make install
#+end_src

* NOTES

** installed <2018-03-31 Sat>
#+begin_src sh :dir /usr/src/kelbt :results output
git log -1
#+end_src

#+RESULTS:
: commit 1de2a1ab2068681c8a1f90a1fed45b5d03852b36
: Author: Greg Nietsky <gregory@distrotech.co.za>
: Date:   Mon May 30 08:48:36 2016 +0200
: 
:     Add -Wno-narrowing to CFLAGS to stop failure on gcc6
