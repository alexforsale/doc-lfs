#+TITLE: scons
#+DATE: <2018-02-24 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:t
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t toc:t todo:t |:t
#+CREATOR: Emacs 25.2.2 (Org mode 8.2.10)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: id
#+SELECT_TAGS: export

* scons

** source

http://www.linuxfromscratch.org/blfs/view/systemd/general/scons.html

** konfigurasi

#+BEGIN_SRC sh
python setup.py install --prefix=/usr  \
                        --standard-lib \
                        --optimize=1   \
                        --install-data=/usr/share
#+END_SRC

** dependencies

*** required

[[file:python-2.org][Python-2.7.14]]

*** optional

[[file:docbook-xsl.org][docbook-xsl-1.79.2]], [[file:libxml2.org][libxml2-2.9.7 (for Python2)]] and [[file:libxslt.org][libxslt-1.1.32]]
(the Python2 modules can be invoked at runtime if using the SCons
Docbook tool) 
