#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: Lasso
#+DATE: <2018-03-06 Tue>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* Lasso

** source

#+BEGIN_SRC sh :dir /usr/src/lasso :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	https://repos.entrouvert.org/lasso.git (fetch)
: origin	https://repos.entrouvert.org/lasso.git (push)

** konfigurasi

#+BEGIN_SRC sh
./configure --prefix=/usr --enable-gtk-doc &&
make &&
sudo make install
#+END_SRC

** dependencies

[[file:xmlsec.org][xmlsec1]]

*** TODO fix build error (konfigurasi di xmlsec)
