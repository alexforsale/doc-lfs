#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: SANE
#+DATE: <2018-03-17 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* sane

** source

*** backend

#+BEGIN_SRC sh :dir /usr/src/sane-project/backends :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	https://gitlab.com/sane-project/backends.git (fetch)
: origin	https://gitlab.com/sane-project/backends.git (push)

*** frontend

#+BEGIN_SRC sh :dir /usr/src/sane-project/frontends :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	https://gitlab.com/sane-project/frontends.git (fetch)
: origin	https://gitlab.com/sane-project/frontends.git (push)

** konfigurasi

#+BEGIN_SRC sh
sudo groupadd -g 70 scanner
#+END_SRC

Jika belum, masukkan user yang akan melakukan kompilasi kedalam group
/scanner/ dan login ulang.

#+BEGIN_SRC sh
sudo usermod -aG scanner <nama user>
su $(whoami)
#+END_SRC

*** backends

*** frontends

** dependencies

*** Optional (Back Ends) 

[[file:avahi.org][Avahi-0.7]], [[file:cups.org][Cups-2.2.6]], [[file:libjpegturbo.org][libjpeg-turbo-1.5.3]], [[file:libtiff.org][LibTIFF-4.0.9]],
[[file:libusb.org][libusb-1.0.21]], [[file:v4l-utils.org][v4l-utils-1.14.2]], Net-SNMP, libieee1284, libgphoto2,
and texlive-20170524 (or install-tl-unx)

*** Optional (Front Ends)

[[file:~/Documents/org/lfs/jalan-panjang-menuju-X.org][X Window System]], [[file:gtk-2.org][GTK+-2.24.32]], and Gimp-2.8.22
