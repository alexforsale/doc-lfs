#+TITLE: polkit
#+DATE: <2018-02-25 Sun>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:t
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t toc:t todo:t |:t
#+CREATOR: Emacs 25.2.2 (Org mode 8.2.10)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: id
#+SELECT_TAGS: export

* polkit

** source

http://www.linuxfromscratch.org/blfs/view/systemd/postlfs/polkit.html

#+BEGIN_SRC sh :dir /usr/src/polkit :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git://anongit.freedesktop.org/polkit (fetch)
: origin	git://anongit.freedesktop.org/polkit (push)

** konfigurasi

#+BEGIN_SRC sh
groupadd -fg 27 polkitd &&
useradd -c "PolicyKit Daemon Owner" -d /etc/polkit-1 -u 27 \
        -g polkitd -s /bin/false polkitd
#+END_SRC

*Jika dalam kondisi chroot*
#+BEGIN_SRC sh
sed -i "s:/sys/fs/cgroup/systemd/:/sys:g" configure

#+END_SRC

#+BEGIN_SRC sh
./autogen.sh
./configure --prefix=/usr                    \
            --sysconfdir=/etc                \
            --localstatedir=/var             \
            --enable-gtk-doc                 \
            --disable-static                 &&
make && make check &&
sudomake install
#+END_SRC

** dependencies

***  Required

[[file:glib.org][GLib-2.54.3]], and [[file:js38.org][js38-38.2.1]] (js24 jika versi git)

*** Recommended

[[file:Linux-PAM.org][Linux-PAM-1.3.0]]

***  Optional (Required if building GNOME)

[[file:gobject-intropection.org][gobject-introspection-1.54.1]]

*** Optional

[[file:docbook-xml.org][docbook-xml-4.5]], [[file:docbook-xsl.org][docbook-xsl-1.79.2]], [[file:gtk-doc.org][GTK-Doc-1.27]], and [[file:libxslt.org][libxslt-1.1.32]]

*** Required Runtime Dependencies

[[file:systemd.org][Systemd-237]]

** konfigurasi polkit

#+BEGIN_SRC sh
cat > /etc/pam.d/polkit-1 << "EOF"
# Begin /etc/pam.d/polkit-1

auth     include        system-auth
account  include        system-account
password include        system-password
session  include        system-session

# End /etc/pam.d/polkit-1
EOF
#+END_SRC
