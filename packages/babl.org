#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: babl
#+DATE: <2018-03-01 Thu>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* babl

** source

http://www.linuxfromscratch.org/blfs/view/systemd/general/babl.html

** konfigurasi

#+BEGIN_SRC sh
./configure --prefix=/usr &&
make &&
sudo make install &&

sudo install -v -m755 -d /usr/share/gtk-doc/html/babl/graphics &&
sudo install -v -m644 docs/*.{css,html} /usr/share/gtk-doc/html/babl &&
sudo install -v -m644 docs/graphics/*.{html,png,svg} /usr/share/gtk-doc/html/babl/graphics
#+END_SRC
