#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: gcc
#+DATE: <2018-03-07 Wed>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* GCC

Alasan mengapa gcc di /compile/ ulang adalah untuk menambah /compiler/
untuk beberapa /languages/ baru.

** source

#+BEGIN_SRC sh :dir /usr/src/gcc :results output
git remote -v &&
git status
#+END_SRC

#+RESULTS:
: origin	git://gcc.gnu.org/git/gcc.git (fetch)
: origin	git://gcc.gnu.org/git/gcc.git (push)
: On branch gcc-7_3_0-release
: nothing to commit, working tree clean

- Jika mengambil dari *HEAD* git repository gcc, versi alpha terakhir
  adalah /8.0.0/.
- Sebaiknya gunakan versi release terakhir, karena versi alpha
  tentunya masih memiliki bug/fitur yang belum tentu kompatibel.
- cek tag dengan perintah =git tag -l= dan checkout ke tag tersebut
  dengan perintah =git checkout <nama-tag-nya>=
- OPSIONAL:
  - checkout ke branch baru dengan nama branch sama dengan nama tag,
    diperlukan jika kita melakukan patching atau modifikasi disource
    sehingga perubahan tersebut tetap tersimpan direpository lokal
    seandainya pindah ke branch/tag lain.

** konfigurasi

- lakukan instalasi [[file:gnat.org][gnat]] terlebih dahulu.

#+BEGIN_SRC sh
case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
  ;;
esac

mkdir build                                          &&
cd    build                                          &&

../configure                                         \
    --prefix=/usr                                    \
    --disable-multilib                               \
    --with-system-zlib                               \
    --enable-languages=c,c++,fortran,go,objc,obj-c++,ada,brig &&
make
#+END_SRC

*test*
#+BEGIN_SRC sh
ulimit -s 32768 &&
make -k check &&
../contrib/test_summary
#+END_SRC

*install sebagai root*
#+BEGIN_SRC sh
make install &&

mkdir -pv /usr/share/gdb/auto-load/usr/lib              &&
mv -v /usr/lib/*gdb.py /usr/share/gdb/auto-load/usr/lib &&

chown -v -R root:root \
    /usr/lib/gcc/*linux-gnu/7.3.0/include{,-fixed}
#+END_SRC

*hapus instalasi gnat*
#+BEGIN_SRC sh
rm -rf /opt/gnat
#+END_SRC

*sebagai user normal, path ini ditentukan ketika kompilasi gnat*
#+BEGIN_SRC sh
export PATH=$PATH_HOLD &&
unset PATH_HOLD
#+END_SRC

*sebagai user root*
#+BEGIN_SRC sh
/usr/libexec/gcc/x86_64-pc-linux-gnu/7.3.0/install-tools/mkheaders
#+END_SRC

** dependencies

*** Recommended 

[[file:dejagnu.org][DejaGnu-1.6.1]], for tests, [[file:gnat.org][gnat]]
