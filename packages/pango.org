#+TITLE: pango
#+DATE: <2018-02-25 Sun>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:t
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t toc:t todo:t |:t
#+CREATOR: Emacs 25.2.2 (Org mode 8.2.10)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: id
#+SELECT_TAGS: export

* pango

** source

http://www.linuxfromscratch.org/blfs/view/systemd/x/pango.html

** konfigurasi

#+BEGIN_SRC sh
mkdir build &&
cd    build &&

meson --prefix=/usr --sysconfdir=/etc -Denable-docs=yes .. &&
ninja
sudo ninja install

#+END_SRC
** dependencies

*** Required

[[file:fontconfig.org][Fontconfig-2.12.6]] (must be built with [[file:freetype.org][FreeType-2.9]] using [[file:harfbuzz.org][HarfBuzz-1.7.5]]) and [[file:glib.org][GLib-2.54.3]]

*** Recommended

[[file:cairo.org][Cairo-1.14.12]] and [[file:~/Documents/org/lfs/jalan-panjang-menuju-X.org][Xorg Libraries]]

*** Optional (Required if building GNOME)

[[file:gobject-intropection.org][gobject-introspection-1.54.1]]

*** Optional

Cantarell fonts (for tests), [[file:gtk-doc.org][GTK-Doc-1.27]], and libthai 

* DONE reinstall setelah instalasi cairo/X
  CLOSED: [2018-02-27 Tue 16:14]
  - State "DONE"       from "TODO"       [2018-02-27 Tue 16:14] \\
    done
* DONE reinstall setelah instalasi libthai
  CLOSED: [2018-03-01 Thu 22:40]
  - State "DONE"       from "TODO"       [2018-03-01 Thu 22:40] \\
    tidak diperlukan
* NOTES:
- symlink file pkg-config dari lib64 ke lib =sudo ln -svf
  /usr/lib64/pkgconfig/pango* /usr/lib/pkgconfig/=
- symlink library pango ke /usr/lib =sudo ln -sv /usr/lib64/libpango*
  /usr/lib/=

Ini karena instalasi /gtk2/ mencari file file tersebut dilokasi diatas.
* DONE reinstall dengan lokasi library yang benar.
  CLOSED: [2018-03-01 Thu 22:40]
  - State "DONE"       from "TODO"       [2018-03-01 Thu 22:40] \\
    symlink sudah cukup, tidak perlu reinstall
