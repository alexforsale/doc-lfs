#+TITLE: libarchive
#+DATE: <2018-02-24 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:t
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t toc:t todo:t |:t
#+CREATOR: Emacs 25.2.2 (Org mode 8.2.10)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: id
#+SELECT_TAGS: export

* libarchive

** source

http://www.linuxfromscratch.org/blfs/view/systemd/general/libarchive.html

#+BEGIN_SRC sh :dir /mnt/sources/libarchive :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	https://github.com/libarchive/libarchive.git (fetch)
: origin	https://github.com/libarchive/libarchive.git (push)

** konfigurasi

#+BEGIN_SRC sh
./configure --prefix=/usr --disable-static &&
make && make check &&
sudo make install
#+END_SRC

*** git repo
#+BEGIN_SRC sh
mkdir -pv build && cd $_ &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
-DBUILD_SHARED_LIBS=ON .. &&
make && make test &&
sudo make install
#+END_SRC
** dependencies

*** optional

[[file:libxml2.org][libxml2-2.9.7]], [[file:lzo.org][LZO-2.10]], and [[file:nettle.org][Nettle-3.4]]

* NOTES

** update <2018-03-23 Fri>

#+BEGIN_SRC sh :dir /mnt/sources/libarchive :results output
git log -1
#+END_SRC

#+RESULTS:
: commit 108251d88fccc41e70a2eac09720ddaab6d96065
: Merge: 77d26b08 25175f9a
: Author: Tim Kientzle <kientzle@acm.org>
: Date:   Sun Feb 25 11:55:52 2018 -0800
: 
:     Merge pull request #993 from ArshanKhanifar/issue-991
:     
:     flush pending chdirs prior to processing mtree files
