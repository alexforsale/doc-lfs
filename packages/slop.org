#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: slop
#+DATE: <2018-03-24 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* slop

** source
#+BEGIN_SRC sh :dir /usr/src/slop :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git@github.com:naelstrof/slop.git (fetch)
: origin	git@github.com:naelstrof/slop.git (push)

** konfigurasi

#+BEGIN_SRC sh
mkdir -pv build && cd $_ &&
cmake -DCMAKE_INSTALL_PREFIX=/usr .. &&
make && 
#+END_SRC

** dependencies

[[file:glm.org][glm]], [[file:glew.org][glew]]

* NOTES

** installed <2018-03-25 Sun>
#+BEGIN_SRC sh :dir /usr/src/slop :results output
git log -1
#+END_SRC

#+RESULTS:
: commit a28a1da87665af0c10c51ce81c20a9495f81be12
: Merge: 5894741 b8e1c48
: Author: Dalton Nell <naelstrof@gmail.com>
: Date:   Sun Mar 11 04:11:53 2018 -0600
: 
:     Merge pull request #94 from SoapGentoo/soname
:     
:     Make SONAME equal to the PROJECT_VERSION

