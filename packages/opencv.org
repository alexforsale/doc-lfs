#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: opencv
#+DATE: <2018-03-02 Fri>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* opencv.org

** source

http://www.linuxfromscratch.org/blfs/view/systemd/general/opencv.html

** konfigurasi

#+BEGIN_SRC sh
mkdir -pv build && cd $_ &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
-DCMAKE_BUILD_TYPE=Release \
-DBUILD_SHARED_LIBS=ON \
-DENABLE_CXX11=ON \
-DBUILD_PERF_TESTS=OFF \
-DBUILD_TESTS=OFF \
-DENABLE_PRECOMPILED_HEADERS=OFF \
-DCMAKE_SKIP_RPATH=ON \
-DBUILD_WITH_DEBUG_INFO=OFF \
-D-DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib/modules/ \
-DOPENCV_ENABLE_NONFREE=ON \
-DBUILD_DOCS=ON \
-DINSTALL_C_EXAMPLES=ON \
-DINSTALL_PYTHON_EXAMPLES=ON \
-Wno-dev  .. &&
make &&
sudo make install
#+END_SRC

** dependencies

*** Required

[[file:cmake.org][CMake-3.10.2]] and [[file:unzip.org][UnZip-6.0]]

*** Recommended 

[[file:FFmpeg.org][FFmpeg-3.4.2]], [[file:gst-plugins-base.org][gst-plugins-base-1.12.4]], [[file:gtk-3.org][GTK+-3.22.28]], [[file:jasper.org][JasPer-2.0.14]],
[[file:libjpegturbo.org][libjpeg-turbo-1.5.3]], [[file:libpng.org][libpng-1.6.34]], [[file:libtiff.org][LibTIFF-4.0.9]], [[file:libwebp.org][libwebp-0.6.1]],
[[file:v4l-utils.org][v4l-utils-1.14.2]], and [[file:xine-lib.org][xine-lib-1.2.8]]

Optional 

[[file:apache-ant.org][apache-ant-1.10.2]], [[file:doxygen.org][Doxygen-1.8.14]], Java-9.0.4, [[file:python-2.org][Python-2.7.14]], Cuda,
[[file:eigen.org][Eigen]], [[file:openexr.org][OpenEXR]], GCD, GDAL, GigEVisionSDK, [[file:jack2.org][JACK]], [[file:libdc1394.org][libdc1394]], [[file:gphoto2.org][libgphoto2]],
[[file:numpy.org][NumPy]], OpenNI, PlanetUML, PvAPI, Threading Building 
Blocks (TBB), UniCap, VTK - The Visualization Toolkit, and XIMEA 

* TODO reinstall dengan dependencies
