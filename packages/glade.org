#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: glade
#+DATE: <2018-03-31 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.9)

* glade

** source
#+BEGIN_SRC sh :dir /usr/src/glade :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	https://gitlab.gnome.org/GNOME/glade.git (fetch)
: origin	https://gitlab.gnome.org/GNOME/glade.git (push)

** konfigurasi
#+BEGIN_SRC sh :dir /usr/src/glade :results output
./autogen.sh --prefix=/usr &&
make &&
sudo make install
#+END_SRC

* NOTES

** TODO reinstall setelah instalasi webkitgtk

** installed <2018-03-31 Sat>
#+BEGIN_SRC sh :dir /usr/src/glade :results output
git log -1
#+END_SRC

#+RESULTS:
: commit 7105287fc5ec97aa3052f4e7ab5a13a7537a5fea
: Author: Марко Костић <marko.m.kostic@gmail.com>
: Date:   Wed Mar 28 20:40:37 2018 +0000
: 
:     Update Serbian translation
:     
:     (cherry picked from commit f65173e5070c4710b16553b6dabfc4e6b4a47594)
