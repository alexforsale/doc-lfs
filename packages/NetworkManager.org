#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: NetworkManager
#+DATE: <2018-02-28 Wed>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* Network Manager

** source

http://www.linuxfromscratch.org/blfs/view/systemd/basicnet/networkmanager.html

#+BEGIN_SRC sh :dir /usr/src/NetworkManager :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git://anongit.freedesktop.org/NetworkManager/NetworkManager (fetch)
: origin	git://anongit.freedesktop.org/NetworkManager/NetworkManager (push)

** konfigurasi

#+BEGIN_SRC 
./autogen.sh &&
CXXFLAGS="-O2 -fPIC"                                        \
./configure --prefix=/usr                                   \
            --sysconfdir=/etc                               \
            --localstatedir=/var                            \
            --with-nmtui                                    \
            --with-udev-dir=/lib/udev                       \
            --with-session-tracking=systemd                 \
            --with-systemdsystemunitdir=/lib/systemd/system \
            --enable-gtk-doc \
            --enable-modify-system \
            --enable-ppp \
            --enable-json-validation \
            --enable-wifi \
            --enable-vala=yes \
            --enable-concheck \
            --enable-polkit=yes \
            --enable-polkit-agent \
            --enable-teamdctl \
            --enable-json-validation \
            --with-wext=yes \
            --with-systemd-journal=yes \
            --with-suspend-resume=systemd \
            --with-dhcpcd=yes \
            --with-resolvconf=yes \
            --docdir=/usr/share/doc/network-manager &&
make &&
sudo make install
#+END_SRC

*root*
#+BEGIN_SRC sh
cat >> /etc/NetworkManager/NetworkManager.conf << "EOF"
[main]
plugins=keyfile
EOF

groupadd -fg 86 netdev &&
/Usr/sbin/usermod -a -G netdev <username>

cat > /usr/share/polkit-1/rules.d/org.freedesktop.NetworkManager.rules << "EOF"
polkit.addRule(function(action, subject) {
    if (action.id.indexOf("org.freedesktop.NetworkManager.") == 0 && subject.isInGroup("netdev")) {
        return polkit.Result.YES;
    }
});
EOF

systemctl enable NetworkManager
systemctl enable NetworkManager-wait-online

#+END_SRC
** dependencies

*** Required 

[[file:dbus-glib.org][dbus-glib-0.110]], [[file:libndp.org][libndp-1.6]], [[file:libnl.org][libnl-3.4.0]], and [[file:NSS.org][NSS-3.35]] 

Recommended 

[[file:curl.org][cURL-7.58.0]], [[file:dhcpcd.org][dhcpcd-7.0.1]] or DHCP-4.4.0 (client only),
[[file:gobject-intropection.org][gobject-introspection-1.54.1]], [[file:iptables.org][Iptables-1.6.2]], [[file:newt.org][newt-0.52.20]] (for
nmtui), [[file:polkit.org][Polkit-0.113+git_2919920+js38]], [[file:pygobject.org][PyGObject-3.26.1]], [[file:systemd.org][Systemd-237]],
[[file:upower.org][UPower-0.99.7]], [[file:vala.org][Vala-0.38.8]], and [[file:wpa_supplicant.org][wpa_supplicant-2.6]] (built with D-Bus
support),

Optional 

[[file:bluez.org][BlueZ-5.48]], [[file:gtk-doc.org][GTK-Doc-1.27]], Qt-5.10.1 (for examples),
[[file:modemmanager.org][ModemManager-1.6.12]], [[file:valgrind.org][Valgrind-3.13.0]], [[file:dnsmasq.org][dnsmasq]], [[file:jansson.org][Jansson]], [[file:libteam.org][libteam]],
[[file:libpsl.org][libpsl]], [[file:PPP.org][PPP]], and [[file:RP-PPPoE.org][RP-PPPoE]]
