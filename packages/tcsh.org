#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: tcsh
#+DATE: <2018-02-26 Mon>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* tcsh

** source

http://www.linuxfromscratch.org/blfs/view/systemd/postlfs/tcsh.html

#+BEGIN_SRC sh :dir /usr/src/tcsh :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git@github.com:tcsh-org/tcsh.git (fetch)
: origin	git@github.com:tcsh-org/tcsh.git (push)

** konfigurasi

#+BEGIN_SRC sh
sed -i 's|SVID_SOURCE|DEFAULT_SOURCE|g' config/linux  &&
sed -i 's|BSD_SOURCE|DEFAULT_SOURCE|g'  config/linux

#+END_SRC

#+BEGIN_SRC sh
./configure --prefix=/usr --bindir=/bin &&

make &&
sh ./tcsh.man2html

#+END_SRC

#+BEGIN_SRC sh
sudo make install install.man &&

sudo ln -v -sf tcsh   /bin/csh &&
sudo ln -v -sf tcsh.1 /usr/share/man/man1/csh.1
#+END_SRC

#+BEGIN_SRC sh
cat >> /etc/shells << "EOF"
/bin/tcsh
/bin/csh
EOF

#+END_SRC

#+BEGIN_SRC sh
cat > ~/.cshrc << "EOF"
# Original at:
# https://www.cs.umd.edu/~srhuang/teaching/code_snippets/prompt_color.tcsh.html

# Modified by the BLFS Development Team.

# Add these lines to your ~/.cshrc (or to /etc/csh.cshrc).

# Colors!
set     red="%{\033[1;31m%}"
set   green="%{\033[0;32m%}"
set  yellow="%{\033[1;33m%}"
set    blue="%{\033[1;34m%}"
set magenta="%{\033[1;35m%}"
set    cyan="%{\033[1;36m%}"
set   white="%{\033[0;37m%}"
set     end="%{\033[0m%}" # This is needed at the end...

# Setting the actual prompt.  Two separate versions for you to try, pick
# whichever one you like better, and change the colors as you want.
# Just don't mess with the ${end} guy in either line...  Comment out or
# delete the prompt you don't use.

set prompt="${green}%n${blue}@%m ${white}%~ ${green}%%${end} "
set prompt="[${green}%n${blue}@%m ${white}%~ ]${end} "

# This was not in the original URL above
# Provides coloured ls
alias ls ls --color=always

# Clean up after ourselves...
unset red green yellow blue magenta cyan yellow white end
EOF
#+END_SRC
