#+TITLE: swig
#+DATE: <2018-02-24 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:t
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t toc:t todo:t |:t
#+CREATOR: Emacs 25.2.2 (Org mode 8.2.10)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: id
#+SELECT_TAGS: export

* swig

** source

http://www.linuxfromscratch.org/blfs/view/systemd/general/swig.html

** konfigurasi

#+BEGIN_SRC sh
./configure --prefix=/usr                      \
            --without-clisp                    \
            --without-maximum-compile-warnings &&
make

make install &&
install -v -m755 -d /usr/share/doc/swig-3.0.12 &&
cp -v -R Doc/* /usr/share/doc/swig-3.0.12

#+END_SRC
** dependencies

***  Required

[[file:pcre-8.org][PCRE-8.41]]

*** Optional

[[file:boost.org][Boost-1.66.0]] for tests, and any of the languages mentioned in the introduction, as run-time dependencies 
