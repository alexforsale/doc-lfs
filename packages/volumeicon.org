#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: volumeicon
#+DATE: <2018-03-24 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* volumeicon

** source

#+BEGIN_SRC sh :dir /usr/src/volumeicon :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git@github.com:Maato/volumeicon.git (fetch)
: origin	git@github.com:Maato/volumeicon.git (push)

** konfigurasi

#+BEGIN_SRC sh
./autogen.sh &&
./configure --prefix=/usr --enable-notify &&
make && 
sudo make install
#+END_SRC

* NOTES

** installed <2018-03-24 Sat>
#+BEGIN_SRC sh :dir /usr/src/volumeicon :results output
git log -1
#+END_SRC

#+RESULTS:
: commit 27be3d4d58d2433d554d0083eadd20b4a6bd4519
: Author: Mahyuddin <yudi.al@gmail.com>
: Date:   Mon Oct 2 16:38:02 2017 +0700
: 
:     Fix Language name
