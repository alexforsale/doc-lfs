#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: libdaemon
#+DATE: <2018-02-27 Tue>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* libdaemon

** source

http://www.linuxfromscratch.org/blfs/view/systemd/general/libdaemon.html

** konfigurasi

#+BEGIN_SRC sh
./configure --prefix=/usr --disable-static &&
make
make -C doc doxygen
sudo make docdir=/usr/share/doc/libdaemon-0.14 install

#+END_SRC

** dependencies

Optional 

[[file:doxygen.org][Doxygen-1.8.14]] and Lynx-2.8.8rel.2 
