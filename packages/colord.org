#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: colord
#+DATE: <2018-02-27 Tue>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* colord

** source

http://www.linuxfromscratch.org/blfs/view/systemd/general/colord.html

#+BEGIN_SRC sh :dir /usr/src/colord :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git://github.com/hughsie/colord.git (fetch)
: origin	git://github.com/hughsie/colord.git (push)

** konfigurasi

#+BEGIN_SRC sh
sudo groupadd -g 71 colord &&
sudo useradd -c "Color Daemon Owner" -d /var/lib/colord -u 71 \
        -g colord -s /bin/false colord
#+END_SRC

#+BEGIN_SRC 
mkdir -pv build &&
cd build &&
meson --prefix=/usr                   \
      --sysconfdir=/etc               \
      --localstatedir=/var            \
      -Dwith-daemon-user=colord       \
      -Denable-vala=true              \
      -Denable-systemd=true           \
      -Denable-libcolordcompat=true   \
      -Denable-argyllcms-sensor=false \
      -Denable-bash-completion=false  \
      -Denable-docs=true              \
      -Denable-man=true ..            &&
ninja
sudo ninja install
#+END_SRC

** dependencies

*** Required 

[[file:dbus.org][dbus-1.12.4]], [[file:glib.org][GLib-2.54.3]], [[file:Little-CMS.org][Little CMS-2.9]], and [[file:sqlite.org][SQLite-3.22.0]]

*** Recommended 

[[file:gobject-intropection.org][gobject-introspection-1.54.1]], [[file:libgudev.org][libgudev-232]], [[file:libgusb.org][libgusb-0.3.0]],
[[file:polkit.org][Polkit-0.113+git_2919920+js38]], [[file:systemd.org][Systemd-237]], and [[file:vala.org][Vala-0.38.8]]

*** Optional 

[[file:docbook-util.org][DocBook-utils-0.6.14]], gnome-desktop-3.26.2 and colord-gtk-0.1.26 (to
build the example tools), [[file:gtk-doc.org][GTK-Doc-1.27]], [[file:libxslt.org][libxslt-1.1.32]], SANE-1.0.27,
ArgyllCMS, and [[file:bash-completion.org][Bash Completion]].

* DONE instal colord-gtk
  CLOSED: [2018-03-20 Tue 21:43]
  - State "DONE"       from "TODO"       [2018-03-20 Tue 21:43] \\
    colord-gtk dependency optional
