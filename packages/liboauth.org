#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: liboauth
#+DATE: <2018-03-09 Fri>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* liboauth

** source
http://www.linuxfromscratch.org/blfs/view/systemd/postlfs/liboauth.html

** konfigurasi

#+BEGIN_SRC sh
patch -Np1 -i ../liboauth-1.0.3-openssl-1.1.0-2.patch
./configure --prefix=/usr --disable-static &&
make &&
sudo make install &&
sudo install -v -dm755 /usr/share/doc/liboauth-1.0.3 &&
sudo cp -rv doc/html/* /usr/share/doc/liboauth-1.0.3
#+END_SRC
** dependencies

*** Required
[[file:curl.org][cURL-7.58.0]]

Optional
[[file:NSS.org][NSS-3.35]] and [[file:doxygen.org][Doxygen-1.8.14]] (to build documentation)
