#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: gjs
#+DATE: <2018-02-28 Wed>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* gjs

** source

http://www.linuxfromscratch.org/blfs/view/systemd/gnome/gjs.html

** konfigurasi

#+BEGIN_SRC sh
./configure --prefix=/usr &&
make &&
sudo make install
#+END_SRC

** dependencies

*** Required 

[[file:cairo.org][Cairo-1.14.12]], [[file:gobject-intropection.org][gobject-introspection-1.54.1]], and [[file:JS-52.2.1gnome1.org][js52-52.2.1gnome1]]

*** Recommended (required for GNOME)

[[file:gtk-3.org][GTK+-3.22.28]]

*** Optional

Gvfs-1.34.2 (for tests), [[file:valgrind.org][Valgrind-3.13.0]] (for tests), DTrace, [[file:LCOV.org][LCOV]],
and Systemtap
