#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: libgd
#+DATE: <2018-03-13 Tue>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* libgd

** source

#+BEGIN_SRC sh :dir /usr/src/libgd :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	https://github.com/libgd/libgd.git (fetch)
: origin	https://github.com/libgd/libgd.git (push)

** konfigurasi

#+BEGIN_SRC sh
mkdir -pv build && cd $_ &&
cmake -DCMAKE_INSTALL_PREFIX=/usr \
      -DBUILD_SHARED_LIBS=ON .. &&
make &&
sudo make install
#+END_SRC

** dependencies
[[file:fontconfig.org][fontconfig]], [[file:libwebp.org][libwebp]], [[file:~/Documents/org/lfs/jalan-panjang-menuju-X.org][libxpm]], [[file:ttf-liberation.org][ttf-liberation]] (check)

* NOTES

** updated <2018-04-01 Sun>
#+BEGIN_SRC sh :dir /usr/src/libgd :results output
git log -1
#+END_SRC

#+RESULTS:
: commit 4a43ee1c30f9aadbb0a78c8aebce5d6a70a491cc
: Author: Christoph M. Becker <cmbecker69@gmx.de>
: Date:   Thu Feb 8 18:20:02 2018 +0100
: 
:     Fix typo
:     
:     Cf. php/php-src@2d48d734a20192a10792669baaa88dbe86f2b3a6.

