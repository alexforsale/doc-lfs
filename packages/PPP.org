#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: PPP
#+DATE: <2018-02-28 Wed>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* PPP

** source

https://www.samba.org/ftp/ppp/

** konfigurasi

#+BEGIN_SRC sh
./configure --prefix=/usr &&
make &&
sudo make install &&
sudo mknod /dev/ppp c 108 0 &&
sudo chmod 600 /dev/ppp
#+END_SRC
