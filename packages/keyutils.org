#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: keyutils
#+DATE: <2018-03-22 Thu>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* keyutils

** source

#+BEGIN_SRC sh :dir /mnt/sources/keyutils :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	git://git.kernel.org/pub/scm/linux/kernel/git/dhowells/keyutils.git (fetch)
: origin	git://git.kernel.org/pub/scm/linux/kernel/git/dhowells/keyutils.git (push)

** konfigurasi

#+BEGIN_SRC sh
make &&
sudo make NO_ARLIB=1 install
#+END_SRC

* NOTES

** installed on <2018-03-22 Thu>

#+BEGIN_SRC sh :dir /mnt/sources/keyutils :results output
git log -1
#+END_SRC

#+RESULTS:
: commit 308119c61e94bcc4c710404b9f679e3bb8316713
: Author: David Howells <dhowells@redhat.com>
: Date:   Wed Mar 15 20:57:15 2017 +0000
: 
:     Move to version 1.5.10
