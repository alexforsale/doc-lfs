#+TITLE: w3m
#+DATE: <2018-02-24 Sat>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:t
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t toc:t todo:t |:t
#+CREATOR: Emacs 25.2.2 (Org mode 8.2.10)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: id
#+SELECT_TAGS: export

* w3m

** source

http://www.linuxfromscratch.org/blfs/view/systemd/basicnet/w3m.html

** konfigurasi

#+BEGIN_SRC sh
patch -Np1 -i ../w3m-0.5.3-bdwgc72-1.patch      &&
sed -i 's/file_handle/file_foo/' istream.{c,h}  &&
sed -i 's#gdk-pixbuf-xlib-2.0#& x11#' configure &&
sed -i '/USE_EGD/s/define/undef/' config.h.in   &&


./configure --prefix=/usr --sysconfdir=/etc  &&
make

sudo make install &&
sudo install -v -m644 -D doc/keymap.default /etc/w3m/keymap &&
sudo install -v -m644    doc/menu.default /etc/w3m/menu &&
sudo install -v -m755 -d /usr/share/doc/w3m-0.5.3 &&
sudo install -v -m644    doc/{HISTORY,READ*,keymap.*,menu.*,*.html} \
                    /usr/share/doc/w3m-0.5.3
#+END_SRC

** dependencies

***  Required

[[file:gc.org][GC-7.6.4]]

*** Optional

[[file:gpm.org][GPM-1.20.7]], imlib2-1.4.10, [[file:gtk-2.org][GTK+-2.24.32]], Imlib (not recommended:
obsolete, abandoned upstream, buggy, and gives no additional
functionality as compared to other image loading libraries),
gdk-pixbuf-2.36.11, [[file:compface.org][Compface-1.5.2]], and nkf, a Mail User Agent, and an
External Browser 

* DONE reinstall setelah instalasi gtk-2
  CLOSED: [2018-03-01 Thu 18:19]
  - State "DONE"       from "TODO"       [2018-03-01 Thu 18:19]
