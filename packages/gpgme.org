#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: gpgme
#+DATE: <2018-03-05 Mon>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* gpgme

** source

http://www.linuxfromscratch.org/blfs/view/systemd/postlfs/gpgme.html

** konfigurasi

#+BEGIN_SRC sh
./configure --prefix=/usr &&
make &&
sudo make install
#+END_SRC

** dependencies

*** Required
[[file:libassuan.org][Libassuan-2.5.1]]

*** Optional
[[file:doxygen.org][Doxygen-1.8.14]] (for API documentation), [[file:gnupg.org][GnuPG-2.2.4]] (required if [[file:QT5.org][Qt]] or
[[file:swig.org][SWIG]] are installed; used during the testsuite), [[file:clisp.org][Clisp-2.49]],
[[file:python-2.org][Python-2.7.14]], [[file:QT5.org][Qt-5.10.1]], and/or [[file:swig.org][SWIG-3.0.12]] (for language bindings)

