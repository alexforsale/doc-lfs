#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: libaio
#+DATE: <2018-03-08 Thu>
#+AUTHOR: Christian Alexander
#+EMAIL: alexforsale@yahoo.com
#+LANGUAGE: id
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 27.0.50 (Org mode 9.1.6)

* libaio

** source
http://ftp.de.debian.org/debian/pool/main/liba/libaio/

#+BEGIN_SRC sh :dir /usr/src/libaio :results output
git remote -v
#+END_SRC

#+RESULTS:
: origin	https://pagure.io/libaio.git (fetch)
: origin	https://pagure.io/libaio.git (push)

** konfigurasi

#+BEGIN_SRC sh
sudo make prefix=/usr install
#+END_SRC

* NOTES

** updated <2018-03-23 Fri>
#+BEGIN_SRC sh :dir /usr/src/libaio :results output
git log -1
#+END_SRC

#+RESULTS:
#+begin_example
commit f66be22ab0a59a39858900ab72a8c6a6e8b0b7ec
Author: Jeff Moyer <jmoyer@redhat.com>
Date:   Tue Mar 6 17:24:47 2018 -0500

    libaio-0.3.111
    
    - Add two new tests to the test harness (Jeff Moyer)
    - Generic arch dectection for padding defines (Nathan Rossi)
    - harness: don't hardcode page size (Jeff Moyer)
    - harness: add a test case for mremap (Jeff Moyer)
    - libaio: harness: fix build errors due to attribute warn_unused_result (Mauricio Faria de Oliveira)
    - libaio: harness: fix build error due to linker search order (Mauricio Faria de Oliveira)
    - harness: add test for allocating aio-max-nr ioctxs (Jeff Moyer)
    - Add support for preadv2/pwritev2 (Jeff Moyer)
    - syscall-generic: don't overwrite errno (Jeff Moyer)
    - syscall: get rid of custom syscall implementation (Jeff Moyer)
    - Change syscall-arm64.h to syscall-generic.h (Icenowy Zheng)
    - Use generic syscall number schema for RISC-V (Icenowy Zheng)
    - Add endian detection (LE) and bit width detection (32/64) for RISC-V (Icenowy Zheng)
    - Makefile: convert tag and archive targets to git (Jeff Moyer)
    
    Signed-off-by: Jeff Moyer <jmoyer@redhat.com>
#+end_example
